<?php

namespace MaintenanceModeHelper\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Illuminate\Support\Facades\Auth;

class CheckForMaintenanceMode extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    protected $except = [

    ];

    public function __construct(Application $app)
    {
        parent::__construct($app);

        $this->except = config("maintenanceMode.exceptURI");

    }

    public function handle($request, Closure $next)
    {



        $allowedGuards = config("maintenanceMode.allowedGuards");

        foreach ($allowedGuards as $guard)
        {

            if(Auth::guard($guard)->check()) return $next($request);

        }




        return parent::handle($request,$next);


    }

}
