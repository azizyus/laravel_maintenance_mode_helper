<?php

namespace MaintenanceModeHelper;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class CheckForMaintenanceModeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $configFileName = "maintenanceMode.php";
        $configPath = __DIR__."/config/";
        $configFullPath = $configPath.$configFileName;
        $this->mergeConfigFrom($configFullPath,"maintenanceMode");

        $this->publishes([

            $configFullPath => config_path($configFileName),

        ],"dd/laravel_maintenance_mode_helper-publish-config");

        ## CHECK MAINTENANCE MODE ##
        $isMaintenance = App::isDownForMaintenance();
        View::share([

            "isMaintenance" => $isMaintenance,

        ]);
        ## CHECK MAINTENANCE MODE ##

    }
}
